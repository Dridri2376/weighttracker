"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Members_services_1 = __importDefault(require("../services/Members.services"));
const bcrypt = __importStar(require("bcryptjs"));
const jwt = __importStar(require("jsonwebtoken"));
const express_1 = require("@decorators/express");
let MembersControllers = class MembersControllers {
    ms;
    constructor() {
        this.ms = new Members_services_1.default();
    }
    async GetAllMembers(req, res) {
        try {
            const listOfAllMembers = await this.ms.findAll();
            if (listOfAllMembers != null && listOfAllMembers.length)
                res.status(200).send({ listOfAllMembers }).end();
            else
                res.status(404).send({ err: "NO MEMBERS FOUND IN DATABASE" }).end();
        }
        catch (error) {
            res.status(500).send(error.message).end();
        }
    }
    async getOneMember(req, res) {
        try {
            let member = await this.ms.findOne(req.params._id);
            if (member != null)
                res.status(200).send({ member }).end();
            else
                res.status(404).send({ err: "NO MEMBERS FOUND IN DATABASE" }).end();
        }
        catch (error) {
            if (error)
                res.status(500).send(error.message).end();
        }
    }
    async addNewMember(req, res) {
        try {
            req.body.memberPassword = bcrypt.hashSync(req.body.memberPassword, 10);
            let newMember = await this.ms.register(req.body);
            if (newMember)
                res.status(201).send(newMember).end();
            else
                res.status(400).send({ err: "BAD REQUEST" }).end();
        }
        catch (error) {
            if (error)
                res.status(500).send(error.message).end();
        }
    }
    async login(req, res) {
        if (!req.body.memberEmail || !req.body.memberPassword)
            res.status(400).send({ err: "BAD REQUEST" }).end();
        try {
            let isMember = await this.ms.login(req.body.memberEmail);
            if (!isMember)
                res.status(404).send("unknown member").end();
            else {
                let correctCredentials = bcrypt.compareSync(req.body.memberPassword, isMember.memberPassword);
                if (correctCredentials) {
                    let token = jwt.sign({ isMember }, process.env.SECRET_KEY);
                    res.status(200).send({ token });
                }
                else {
                    res.status(404).send("Incorrect email or password");
                }
            }
        }
        catch (error) {
            res.status(500).send(error.message);
        }
    }
    async getMe(req, verifyToken, res) {
        try {
            const me = await this.ms.findOne(req.params._id);
            console.log(me);
            res.status(200).send({ me }).end();
        }
        catch (error) {
            res.status(500).send(error.message).end();
        }
    }
};
__decorate([
    (0, express_1.Get)("/"),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], MembersControllers.prototype, "GetAllMembers", null);
__decorate([
    (0, express_1.Get)("/:_id"),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], MembersControllers.prototype, "getOneMember", null);
__decorate([
    (0, express_1.Post)("/register"),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], MembersControllers.prototype, "addNewMember", null);
__decorate([
    (0, express_1.Post)("/member/login"),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], MembersControllers.prototype, "login", null);
__decorate([
    (0, express_1.Get)("/member/me"),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object]),
    __metadata("design:returntype", Promise)
], MembersControllers.prototype, "getMe", null);
MembersControllers = __decorate([
    (0, express_1.Controller)("/members"),
    __metadata("design:paramtypes", [])
], MembersControllers);
exports.default = MembersControllers;
//# sourceMappingURL=Members.controllers.js.map