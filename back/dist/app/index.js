"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("dotenv").config();
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
const morgan_1 = __importDefault(require("morgan"));
const db_1 = require("../db/connection/db");
const Members_controllers_1 = __importDefault(require("../controllers/Members.controllers"));
const express_2 = require("@decorators/express");
// server instances and config
const app = (0, express_1.default)();
app.use((0, cors_1.default)());
app.use(express_1.default.json());
app.use((0, morgan_1.default)("dev"));
//router
const router = express_1.default.Router();
(0, express_2.attachControllers)(router, [Members_controllers_1.default]);
app.use("/api", router);
// db connection
(0, db_1.dbConnection)();
// server entryPoint
app.listen(process.env.PORT, () => console.info(`Connected to db on port ${process.env.PORT}`));
//# sourceMappingURL=index.js.map