"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongodb_1 = require("mongodb");
const db_1 = require("../db/connection/db");
class MembersServices {
    constructor() { }
    async findAll() {
        try {
            let allMembers = await db_1.members.find({}).toArray();
            return allMembers;
        }
        catch (error) {
            throw Error(error.message);
        }
    }
    async findOne(_id) {
        try {
            let oneMember = await db_1.members.findOne({
                _id: new mongodb_1.ObjectId(_id),
            });
            return oneMember;
        }
        catch (error) {
            throw Error(error.message);
        }
    }
    async register(member) {
        try {
            let newMember = await db_1.members.insertOne(member);
            return newMember;
        }
        catch (error) {
            throw Error(error.message);
        }
    }
    async login(memberEmail) {
        try {
            let authenticatedMember = await db_1.members.findOne({
                memberEmail,
            });
            return authenticatedMember;
        }
        catch (error) {
            throw Error(error.message);
        }
    }
}
exports.default = MembersServices;
//# sourceMappingURL=Members.services.js.map