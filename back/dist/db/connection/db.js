"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.dbConnection = exports.weightSnapShot = exports.members = void 0;
const mongodb_1 = require("mongodb");
const uri = process.env.URI;
const client = new mongodb_1.MongoClient(uri);
const dbName = process.env.DB;
exports.members = null;
exports.weightSnapShot = null;
const dbConnection = async () => {
    try {
        await client.connect();
        await client.db(dbName);
        console.log("Connected successfully to " + client.db(dbName).databaseName);
        exports.members = client.db(dbName).collection(process.env.MEMBERS);
        exports.weightSnapShot = client.db(dbName).collection(process.env.WEIGHTSNAPSHOT);
    }
    catch (error) {
        console.error(error.message);
        return error;
    }
};
exports.dbConnection = dbConnection;
//# sourceMappingURL=db.js.map