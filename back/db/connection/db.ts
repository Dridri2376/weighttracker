import { MongoClient } from "mongodb";

const uri: string = process.env.URI;
const client = new MongoClient(uri);
const dbName = process.env.DB;

export let members: any = null;
export let weightSnapShot: any = null;

export const dbConnection = async () => {
  try {
    await client.connect();
    await client.db(dbName);
    console.log("Connected successfully to " + client.db(dbName).databaseName);
    members = client.db(dbName).collection(process.env.MEMBERS);
    weightSnapShot = client.db(dbName).collection(process.env.WEIGHTSNAPSHOT);
  } catch (error) {
    console.error(error.message);
    return error;
  }
};
