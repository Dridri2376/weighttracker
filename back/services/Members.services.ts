import { ObjectId } from "mongodb";
import IMember from "../models/Members.model";
import IMemberActions from "../actions/Members.actions";
import { members, weightSnapShot } from "../db/connection/db";
export default class MembersServices implements IMemberActions {
  constructor() {}

  public async findAll(): Promise<IMember[]> {
    try {
      let allMembers: IMember[] = await members.find({}).toArray();
      return allMembers;
    } catch (error) {
      throw Error(error.message);
    }
  }

  public async findOne(_id: any): Promise<IMember> {
    try {
      let oneMember: IMember = await members.findOne({
        _id: new ObjectId(_id),
      });
      return oneMember;
    } catch (error) {
      throw Error(error.message);
    }
  }

  public async register(member: IMember): Promise<IMember> {
    try {
      let newMember = await members.insertOne(member);
      return newMember;
    } catch (error) {
      throw Error(error.message);
    }
  }

  public async login(memberEmail: string): Promise<IMember> {
    try {
      let authenticatedMember: IMember = await members.findOne({
        memberEmail,
      });
      return authenticatedMember;
    } catch (error) {
      throw Error(error.message);
    }
  }
}
