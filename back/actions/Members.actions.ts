import { ObjectId } from 'mongodb';
import IMember from '../models/Members.model';

export default interface IMemberActions
{
    findAll (): Promise<IMember[]>;
    findOne ( _id: any ): Promise<IMember>;
    register ( member: IMember ): Promise<any>;
    login ( email: string ): Promise<any>;

}
