import IMember from "../models/Members.model";
import MembersServices from "../services/Members.services";
import * as bcrypt from "bcryptjs";
import { Request, Response } from "express";
import * as jwt from "jsonwebtoken";
import { Controller, Get, Post, Put, Delete } from "@decorators/express";
import { verifyToken } from "../utils/middleware/auth_middleware";

@Controller("/members")
export default class MembersControllers {
  private ms: MembersServices;

  constructor() {
    this.ms = new MembersServices();
  }

  @Get("/")
  public async GetAllMembers(req: Request, res: Response) {
    try {
      const listOfAllMembers = await this.ms.findAll();
      if (listOfAllMembers != null && listOfAllMembers.length)
        res.status(200).send({ listOfAllMembers }).end();
      else res.status(404).send({ err: "NO MEMBERS FOUND IN DATABASE" }).end();
    } catch (error) {
      res.status(500).send(error.message).end();
    }
  }

  @Get("/:_id")
  public async getOneMember(req: Request, res: Response) {
    try {
      let member = await this.ms.findOne(req.params._id);
      if (member != null) res.status(200).send({ member }).end();
      else res.status(404).send({ err: "NO MEMBERS FOUND IN DATABASE" }).end();
    } catch (error) {
      if (error) res.status(500).send(error.message).end();
    }
  }

  @Post("/register")
  public async addNewMember(req: Request, res: Response) {
    try {
      req.body.memberPassword = bcrypt.hashSync(req.body.memberPassword, 10);
      let newMember = await this.ms.register(req.body);
      if (newMember) res.status(201).send(newMember).end();
      else res.status(400).send({ err: "BAD REQUEST" }).end();
    } catch (error) {
      if (error) res.status(500).send(error.message).end();
    }
  }

  @Post("/member/login")
  public async login(req: Request, res: Response) {
    if (!req.body.memberEmail || !req.body.memberPassword)
      res.status(400).send({ err: "BAD REQUEST" }).end();
    try {
      let isMember = await this.ms.login(req.body.memberEmail);
      if (!isMember) res.status(404).send("unknown member").end();
      else {
        let correctCredentials = bcrypt.compareSync(
          req.body.memberPassword,
          isMember.memberPassword
        );
        if (correctCredentials) {
          let token = jwt.sign({ isMember }, process.env.SECRET_KEY);
          res.status(200).send({ token });
        } else {
          res.status(404).send("Incorrect email or password");
        }
      }
    } catch (error) {
      res.status(500).send(error.message);
    }
  }

  @Get("/member/me")
  public async getMe(req: Request, verifyToken, res: Response) {
    try {
      const me = await this.ms.findOne(req.params._id);
      console.log(me);
      res.status(200).send({ me }).end();
    } catch (error) {
      res.status(500).send(error.message).end();
    }
  }
}
