export default interface IMember {
  _id: string;
  memberFirstName: string;
  memberLastName: string;
  memberAge: number;
  memberEmail: string;
  memberPassword: string;
  memberWeights: number[];
  memberPhone: number;
}
