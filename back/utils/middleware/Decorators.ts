import { Middleware } from "@decorators/express";
import { NextFunction, Request, Response } from "express";

export default class MemberMiddleware implements Middleware {
  public use(request: Request, response: Response, next: NextFunction): void {
    next();
  }
}
