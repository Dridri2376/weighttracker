import * as jwt from "jsonwebtoken";

export const verifyToken = (req, res, next) => {
  const token = req.headers("token");
  if (!token)
    return res.status(403).json({
      message: "Access denied",
    });

  try {
    const decoded = jwt.verify(token, process.env.SECRET_KEY);
    req.member = decoded;
    next();
  } catch (e) {
    res.status(400).send({ message: e.message || "invalid Token" });
  }
};
