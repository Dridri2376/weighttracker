require("dotenv").config();
import express from "express";
import cors from "cors";
import morgan from "morgan";
import { dbConnection } from "../db/connection/db";
import MembersControllers from "../controllers/Members.controllers";
import { attachControllers } from "@decorators/express";

// server instances and config
const app = express();
app.use(cors());
app.use(express.json());
app.use(morgan("dev"));

//router
const router = express.Router();
attachControllers(router, [MembersControllers]);
app.use("/api", router);

// db connection
dbConnection()

// server entryPoint
app.listen(process.env.PORT, () =>
  console.info(`Connected to db on port ${process.env.PORT}`)
);
